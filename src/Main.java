

import java.util.Scanner;

public class Main {

    //    1. Money Exchange App
//    Se va afisa meniul urmator iar dupa fiecare optiune aleasa se va intreba utilizatorul daca doreste sa efectueze o noua operatie.Daca raspunde cu y -> se va afisa din nou meniul, cu n se va parasi aplicatia.
//1.EURO -> USD
//2.USD -> EURO
//3.RON -> EURO
//4.EURO -> RON
//5.Exit
//    Se va citi de la tastatura in startul programului rata de schimb pentru fiecare moneda!
    public static void main(String[] args) {
        Scanner inputNumere = new Scanner(System.in);
        System.out.print("Introdu cursul EURO-USD: ");
        double euroToUsd = inputNumere.nextDouble();
        System.out.print("Introdu cursul USD-EURO: ");
        double usdToEuro = inputNumere.nextDouble();
        System.out.print("Introdu cursul RON-EURO: ");
        double ronToEuro = inputNumere.nextDouble();
        System.out.print("Introdu cursul EURO-RON: ");
        double euroToRon = inputNumere.nextDouble();
        Exchange exchange = new Exchange(euroToUsd, usdToEuro, ronToEuro, euroToRon);
        exchange.startExchange();
    }
}
