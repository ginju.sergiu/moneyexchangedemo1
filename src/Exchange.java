import java.util.Scanner;

public class Exchange {
    private double euroToUsd;
    private double usdToEuro;
    private double ronToEuro;
    private double euroToRon;
    private Scanner inputNumere=new Scanner(System.in);
    private Scanner inputText=new Scanner(System.in);


    public Exchange(double euroToUsd, double usdToEuro, double ronToEuro, double euroToRon) {
        this.euroToUsd = euroToUsd;
        this.usdToEuro = usdToEuro;
        this.ronToEuro = ronToEuro;
        this.euroToRon = euroToRon;
    }

    public void afisareMeniu(){
        System.out.println("===MENIU===");
        System.out.println("1.EURO -> USD");
        System.out.println("2.USD -> EURO");
        System.out.println("3.RON -> EURO");
        System.out.println("4.EURO -> RON");
        System.out.println("5.Exit");
    }

    public void startExchange() {
        int optiuneAleasa;
        String raspuns = "n";
        do {
            afisareMeniu();
            System.out.print("Alege o optiune din meniu:");
            optiuneAleasa = inputNumere.nextInt();
            switch (optiuneAleasa) {
                case 1:
                    euroToUsd();
                    break;
                case 2:
                    usdToEuro();
                    break;
                case 3:
                    ronToEuro();
                    break;
                case 4:
                    euroToRon();
                    break;
                case 5:
                    System.out.println("Multumim! O zi frumoasa.");
                    break;
                default:
                    System.out.println("Nu ai ales optiunea valida");
            }
            if(optiuneAleasa != 5) {
                System.out.print("Doriti sa mai efectuati o optiune? (y/n) :");
                raspuns = inputText.nextLine();
            }else {
                raspuns = "n";
            }
        }while(raspuns.equalsIgnoreCase("y"));
    }

    private void euroToUsd(){
        System.out.print("Intordu suma in euro (suma intreaga) pe care doresti sa o schimbi in USD: ");
        int sumaIntrodusa = (int) inputNumere.nextDouble();
        double rezultatInDolari = sumaIntrodusa * euroToUsd;
        System.out.println("Poftim " + rezultatInDolari + " USD");
    }

    private void usdToEuro(){
        System.out.print("Intordu suma in usd (suma intreaga) pe care doresti sa o schimbi in euro: ");
        int sumaIntrodusa = (int) inputNumere.nextDouble();
        double rezultatInEuro = sumaIntrodusa *usdToEuro;
        System.out.println("Poftim " + rezultatInEuro + " Euro");
    }

    private void ronToEuro(){
        System.out.print("Intordu suma in ron (suma intreaga) pe care doresti sa o schimbi in euro: ");
        int sumaIntrodusa = (int) inputNumere.nextDouble();
        double rezultatInEuro = sumaIntrodusa * ronToEuro;
        System.out.println("Poftim " + rezultatInEuro + " Euro");
    }

    private void euroToRon(){
        System.out.print("Intordu suma in euro (suma intreaga) pe care doresti sa o schimbi in ron: ");
        int sumaIntrodusa = (int) inputNumere.nextDouble();
        double rezultatInRon = sumaIntrodusa * euroToRon;
        System.out.println("Poftim " + rezultatInRon + " ron");

    }


    public double getEuroToUsd() {
        return euroToUsd;
    }

    public void setEuroToUsd(double euroToUsd) {
        this.euroToUsd = euroToUsd;
    }

    public double getUsdToEuro() {
        return usdToEuro;
    }

    public void setUsdToEuro(double usdToEuro) {
        this.usdToEuro = usdToEuro;
    }

    public double getRonToEuro() {
        return ronToEuro;
    }

    public void setRonToEuro(double ronToEuro) {
        this.ronToEuro = ronToEuro;
    }

    public double getEuroToRon() {
        return euroToRon;
    }

    public void setEuroToRon(double euroToRon) {
        this.euroToRon = euroToRon;
    }
}
